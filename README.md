# Jedi Knight: Jedi Academy map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Jedi Knight: Jedi Academy.

This gamepack is based on the game pack from https://svn.icculus.org/gtkradiant-gamepacks/JAPack
